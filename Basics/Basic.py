from flask import Flask, jsonify, request, make_response
import flask
import pymongo

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST', 'PUT'])
def hello_world():
    a1 = request.args.get('agrg1')
    print(a1)
    if request.method == 'GET':
        return flask.jsonify({'aaa': 'bbb'})

    if request.method == 'POST':
        print(request.url)
        return make_response("Successfull", 200)

    if request.method == 'PUT':
        return make_response("Successfull-2", 200)


@app.route('/read', methods=['GET'])
def get():
    if request.method == 'GET':
        return flask.jsonify({'demo': 'bbb'})


if __name__ == '__main__':
    app.run(debug=True)
    client=pymongo.MongoClient('localhost', 27017)
    print("ol",client)
