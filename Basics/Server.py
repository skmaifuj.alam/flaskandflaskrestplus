import flask
import pymongo
from flask import request, Flask
import json
app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World"


@app.route("/dbs/", methods=['GET'])
def dbs():
    client = pymongo.MongoClient('localhost', 27017)
    xx = list(client.database_names())
    client.close()
    return flask.render_template('dbs.html', result=xx)


@app.route("/getd/")
def getd():
    client = pymongo.MongoClient('localhost', 27017)
    record = client['d1']['c1'].find({}, {'_id': False})  # Filtering
    final_record = list(record)  # Its a cursor and need to translate to list
    xx = []
    # print(final_record)
    for i in final_record:
        for k, v in i.items():
            # print(k, v)
            xx.append({k, v})
    # print(xx)
    client.close()
    return str(xx)


@app.route('/postd/',methods=['POST'])
def postd():
    client = pymongo.MongoClient()
    # new_data = {"Key1": "Value1"}
    data = request.get_data()
    obj = json.loads(data)
    # ack = client['d1']['c1'].insert_one(new_data)  # Ack is Acknowledgement
    client['d1']['c1'].insert_one({"names":obj.get("names",""),"address":obj.get("address","")})
    #return flask.redirect('/getd/')
    return "You have successfully insert the data"

@app.route('/setd/')
def setd():
    client = pymongo.MongoClient('localhost', 27017)
    query = {"Key1": "value1"}
    newvalues = {"$set": {"Key1": "new_value"}}

    ack = client['d1']['c1'].update_many(query, newvalues)  # Ack is Acknowledgement
    return str(ack.modified_count) + " Values Updated"


if __name__ == '__main__':
    app.run(debug=True,port=3300)