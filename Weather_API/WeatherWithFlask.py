# from flask import Flask
# from flask import Flask
# from flask import render_template
import flask
import pytest
import requests

url = 'http://api.openweathermap.org/data/2.5/weather?q=Pune&appid=bed5975bf04e5540769c66d9af9e3873&units=metric'
res = requests.get(url)
data = res.json()
temp = data['main']['temp']
wind_speed = data['wind']['speed']
latitude = data['coord']['lat']
longitude = data['coord']['lon']
description = data['weather'][0]['description']
ss = "Place: " + data['name'] + "\n" + "Latitude:" + str(latitude) + "\n" + "Longitude:" + str(
    longitude) + "\n" + "Description:" + description + "\nTemperature:" + str(temp) + " C\n" + "Wind Speed:" + str(
    wind_speed) + "Km/Hr"


app = flask.Flask(__name__)
@app.route('/')
def hello_world():
    return 'H!'


@app.route('/weather')
def weather():
    return ss


@app.route('/welcome')
def welcome():
    return flask.render_template("welcome.html")


@app.route('/data')
def datas():
    return data

if __name__ == '__main__':
    app.run(debug=True)
